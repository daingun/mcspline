examples = graph \
	line

.PHONY : all all_examples check-format clippy doc $(examples)

# Run check, tests and examples
all:
	cargo c && cargo t && make all_examples

# Run all examples
all_examples: $(examples)

# Check code format, does not apply changes
check-format:
	cargo fmt --all -- --check

# Clippy linting for code, tests and examples with pedantic lints
clippy:
	cargo clippy --all-targets -- -W clippy::pedantic

# Create documentation without dependencies.
doc:
	cargo doc --no-deps

# '$@' is the name of the target
$(examples):
	cargo run --example $@

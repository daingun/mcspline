# Monotonic Cubic Spline Interpolation

Third order polynomial interpolation that guarantees the monotonicity
of the interpolated data.

[Souce code repository](https://gitlab.com/daingun/mcspline)

[Crate registry](https://crates.io/crates/mcspline)

[Software specification and API documentation](https://docs.rs/mcspline)

use mcspline::Mcspline;

#[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]
fn main() {
    let xs = vec![0.0, 1.5, 2.0, 3.0, 4.0, 5.0];
    let ys = vec![0.0, 2.0, 2.0, 4.0, 3.0, 2.0];
    let mcs = Mcspline::new(&xs, &ys).unwrap();

    for i in 0..=50 {
        let x = f64::from(i) / 10.0;
        let point = mcs.get(x).unwrap();
        let y = point / 4.0 * 60.0;
        println!("{x:.2}{c:>w$}", x = x, w = y as i64 as usize, c = "+");
    }
}

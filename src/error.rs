//! Error representation

use std::{error, fmt};

///  Custom `Err` variant of `std::Result`
pub struct Error {
    /// Internal representation
    repr: Repr,
}

/// Internal representation of `Error`
#[derive(Debug)]
enum Repr {
    /// Not enough data for the interpolation, there shall be at least three points
    NotEnoughData,
    /// The two arguments have a different length,
    /// the enum holds the length of the two slices
    UnmatchedLength(usize, usize),
}

/// Kind of `Error`
#[allow(clippy::module_name_repetitions)]
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ErrorKind {
    /// Not enough data for the interpolation
    NotEnoughData,
    /// The two arguments have a different length
    UnmatchedLength,
}

impl Error {
    /// Create an `Error` of kind `NotEnoughData`
    pub(crate) fn not_enugh_data() -> Self {
        Error {
            repr: Repr::NotEnoughData,
        }
    }

    /// Create an `Error` of kind `UnmatchedLength`
    ///
    /// # Arguments
    ///
    /// * `x` - length of the first argument
    /// * `y` - length of the second argument
    pub(crate) fn unmatched_length(x: usize, y: usize) -> Self {
        Error {
            repr: Repr::UnmatchedLength(x, y),
        }
    }

    /// Returns the representation of the kind of error
    #[must_use]
    pub fn kind(&self) -> ErrorKind {
        match self.repr {
            Repr::NotEnoughData => ErrorKind::NotEnoughData,
            Repr::UnmatchedLength(_, _) => ErrorKind::UnmatchedLength,
        }
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.repr {
            Repr::NotEnoughData => {
                write!(f, "Interpolation can be performed on three or more points")
            }
            Repr::UnmatchedLength(xl, yl) => write!(f, "Unmatched length, xs: {}, ys: {}", xl, yl),
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.repr, f)
    }
}

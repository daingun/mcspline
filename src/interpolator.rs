//! Monotonic cubic spline interpolator

use crate::{error::Error, Ops};

const MINIMUM_NUMBER_OF_POINTS: usize = 3;

/// Monotonic cubic spline
#[derive(Debug, Clone)]
pub struct Mcspline<T> {
    xs: Vec<T>,
    ai: Vec<T>,
    bi: Vec<T>,
    ci: Vec<T>,
    di: Vec<T>,
}

impl<T> Mcspline<T>
where
    T: Ops<T>,
{
    /// Create a new Monotonic Cubic Spline
    ///
    /// # Arguments
    ///
    /// * `xs` - Abscissa points
    /// * `ys` - Ordinate points
    ///
    /// # Errors
    ///
    /// Returns an error if slices have different lengths or the number of points
    /// is less than three.
    pub fn new(xs: &[T], ys: &[T]) -> Result<Self, Error> {
        check_length_preconditions(xs.len(), ys.len())?;

        let hs = adjacent_differences(&xs);

        let ss = secants(&ys, &hs);

        let ps = weighted_secants(&ss, &hs);

        let y1 = first_order_coeff(&ss, &ps, &hs);

        let ai = third_order_coeff(&y1, &ss, &hs);

        let bi = second_order_coeff(&y1, &ss, &hs);

        Ok(Self {
            xs: xs.to_vec(),
            ai,
            bi,
            ci: y1,
            di: ys.to_vec(),
        })
    }

    /// Interpolate spline. If the given point is outside the limits of the
    /// interpolation points it returns None.
    ///
    /// # Arguments
    ///
    /// * `x` - Interpolation point
    pub fn get(&self, x: T) -> Option<T> {
        match (self.xs.first(), self.xs.last()) {
            (None, _) | (_, None) => return None,
            (Some(f), Some(l)) => {
                if &x < f || &x > l {
                    return None;
                }
            }
        }
        Some(self.get_unbound(x))
    }

    /// Interpolate spline. It does not check limits of interpolation points.
    ///
    /// # Arguments
    ///
    /// * `x` - Interpolation point
    pub fn get_unbound(&self, x: T) -> T {
        // ai and bi have one less element than di and ci.
        let i = self
            .xs
            .iter()
            .position(|j| j > &x)
            .unwrap_or(self.xs.len() - 1)
            - 1;
        let xi = &self.xs[i];

        let dx = x - xi;
        ((self.ai[i].mul_add(&dx, &self.bi[i])).mul_add(&dx, &self.ci[i])).mul_add(&dx, &self.di[i])
    }
}

/// Check preconditions on data length
///
/// # Arguments
///
/// * `x_len` - Abscissa points length
/// * `y_len` - Ordinate points length
///
/// # Errors
///
/// Returns an error if slices have different lengths or the number of points
/// is less than three.
fn check_length_preconditions(x_len: usize, y_len: usize) -> Result<(), Error> {
    if x_len != y_len {
        return Err(Error::unmatched_length(x_len, y_len));
    }
    if x_len < MINIMUM_NUMBER_OF_POINTS || y_len < MINIMUM_NUMBER_OF_POINTS {
        return Err(Error::not_enugh_data());
    }
    Ok(())
}

/// Adjacent differences.
/// [a, b, c, d] = [b-a, c-b, d-c]
///
/// # Arguments
///
/// * `xs` - Abscissa points
fn adjacent_differences<T>(xs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    xs.windows(2).map(|x| x[1].clone() - &x[0]).collect()
}

/// Secant slopes.
/// s = (y[i+1] - y[i]) / h
///
/// # Arguments
///
/// * `ys` - Ordinate points
/// * `hs` - Differences between abscissa points
fn secants<T>(ys: &[T], hs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    ys.windows(2)
        .zip(hs)
        .map(|(y, h)| (y[1].clone() - &y[0]) / h)
        .collect()
}

/// Weighted secants.
/// (s[i]*h[i+i] + s[i+i]*h[i]) / (h[i] + h[i+1])
///
/// # Arguments
///
/// * `ss` - Secants
/// * `hs` - Differences between abscissa points
fn weighted_secants<T>(ss: &[T], hs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    ss.windows(2)
        .zip(hs.windows(2))
        .map(|(s, h)| (s[0].clone() * &h[1] + s[1].clone() * &h[0]) / (h[0].clone() + &h[1]))
        .collect()
}

/// First order coefficients of spline, end points are zeros.
/// Derivative of spline at control points.
///
/// # Arguments
///
/// * `ss` - Secants
/// * `ps` - Weighted Secants
fn internal_first_order_coeff<T>(ss: &[T], ps: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    let two = T::one() + T::one();
    let internal = ss.windows(2).zip(ps).map(|(s, p)| {
        (s[0].signum() + s[1].signum()) * s[0].abs().min(s[1].abs()).min(p.abs() / &two)
    });
    std::iter::once(T::zero())
        .chain(internal)
        .chain(std::iter::once(T::zero()))
        .collect()
}

/// Derivative at boundary.
///
/// # Arguments
///
/// * `end` - End point: end(secant, interval)
/// * `next` - Next to end point: next(secant, interval)
fn end_points<T>(end: (T, T), next: (T, T)) -> T
where
    T: Ops<T>,
{
    let s_end = end.0.clone();
    let s_next = next.0.clone();
    let h_end = end.1;
    let h_next = next.1;

    let one = T::one();
    let two = one.clone() + &one;

    let p_end = s_end.clone() * (one + h_end.clone() / (h_end.clone() + &h_next))
        - s_next * &h_end / (h_end + h_next);

    if p_end.clone() * &s_end <= T::zero() {
        T::zero()
    } else if p_end.abs() > s_end.abs() * &two {
        two * s_end
    } else {
        p_end
    }
}

/// First order coefficients of spline.
/// Derivative of spline at control points.
///
/// # Arguments
///
/// * `ss` - Secants
/// * `ps` - Weighted Secants
/// * `hs` - Differences between abscissa points
fn first_order_coeff<T>(ss: &[T], ps: &[T], hs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    let mut y1 = internal_first_order_coeff(&ss, &ps);
    let y1_0 = end_points(
        (ss[0].clone(), hs[0].clone()),
        (ss[1].clone(), hs[1].clone()),
    );
    y1[0] = y1_0;
    let last = hs.len();
    let y1_last = end_points(
        (ss[last - 1].clone(), hs[last - 1].clone()),
        (ss[last - 2].clone(), hs[last - 2].clone()),
    );
    y1[last] = y1_last;
    y1
}

/// Third order coefficients for spline.
/// a[i] = (y1[i] + y1[i+1] - 2*s[i]) / h[i]*h[i]
///
/// # Arguments
///
/// * `y1` - First derivative
/// * `ss` - Secants
/// * `hs` - Differences between abscissa points
fn third_order_coeff<T>(y1: &[T], ss: &[T], hs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    let two = T::one() + T::one();
    y1.windows(2)
        .zip(ss)
        .zip(hs)
        .map(|((y, s), h)| (y[0].clone() + &y[1] - two.clone() * s) / (h.clone() * h))
        .collect()
}

/// Second order coefficients for spline.
/// b[i] = (3*s[i] - 2*y1[i] - y1[i+1]) / h[i]
///
/// # Arguments
///
/// * `y1` - First derivative
/// * `ss` - Secants
/// * `hs` - Differences between abscissa points
fn second_order_coeff<T>(y1: &[T], ss: &[T], hs: &[T]) -> Vec<T>
where
    T: Ops<T>,
{
    let two = T::one() + T::one();
    let three = T::one() + &two;
    y1.windows(2)
        .zip(ss)
        .zip(hs)
        .map(|((y, s), h)| (three.clone() * s - two.clone() * &y[0] - y[1].clone()) / h)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::error::ErrorKind;

    #[test]
    fn different_length() {
        let xs = vec![0.0, 1.5, 2.0];
        let ys = vec![0.0, 2.0];
        let mcs = Mcspline::new(&xs, &ys);
        assert_eq!(mcs.unwrap_err().kind(), ErrorKind::UnmatchedLength);
    }

    #[test]
    fn empty_vector() {
        let xs: Vec<f32> = vec![];
        let ys = vec![];
        let mcs = Mcspline::new(&xs, &ys);
        assert_eq!(mcs.unwrap_err().kind(), ErrorKind::NotEnoughData);
    }

    #[test]
    fn monotonic_test() {
        let xs = vec![0.0_f32, 1.5, 2.0, 3.0, 4.0, 5.0];
        let ys = vec![0.0, 2.0, 2.0, 4.0, 3.0, 2.0];
        let mcs = Mcspline::new(&xs, &ys).unwrap();
        assert_eq!(mcs.get(0.0), Some(0.0));
        assert_eq!(mcs.get(1.0), Some(1.740_740_8));
        assert_eq!(mcs.get(1.4), Some(1.989_037));
        assert_eq!(mcs.get(1.5), Some(2.0));
        assert_eq!(mcs.get(1.7), Some(2.0));
        assert_eq!(mcs.get(1.9), Some(2.0));
        assert_eq!(mcs.get(2.0), Some(2.0));
        assert_eq!(mcs.get(3.0), Some(4.0));
        assert_eq!(mcs.get(3.6), Some(3.496));
        assert_eq!(mcs.get(4.0), Some(3.0));
        assert_eq!(mcs.get(4.1), Some(2.9));
        assert_eq!(mcs.get(5.0), Some(2.0));
        assert_eq!(mcs.get(5.1), None);
        assert_eq!(mcs.get(-0.1), None);
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn flat_test() {
        let xs = vec![0.0, 1.0, 2.0, 3.0];
        let ys = vec![0.0, 2.0, 2.0, 0.0];
        let mcs = Mcspline::new(&xs, &ys).unwrap();
        assert_eq!(mcs.get_unbound(1.0), 2.0);
        assert_eq!(mcs.get_unbound(1.1), 2.0);
        assert_eq!(mcs.get_unbound(1.5), 2.0);
        assert_eq!(mcs.get_unbound(1.9), 2.0);
        assert_eq!(mcs.get_unbound(2.0), 2.0);
    }
}

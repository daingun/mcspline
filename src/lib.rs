//! # Monotonic Cubic Spline Interpolation
//!
//! Third order polynomial interpolation that guarantees the monotonicity
//! of the interpolated data.
//!
//! # Functional Requirements (FR)
//!
//! **FR1** - The piecewise interpolating function is composed by polynomials
//! whose degree is at most three.
//!
//! **FR2** - The interpolating polynomials are guaranteed to be monotonic.
//!
//! **FR3** - The interpolating polynomials are continuous up to the first derivative
//!
//! **FR4** - In order to build the interpolation, the algorithm requires the data
//! to be supplied as two slices representing abscissa and ordinate values.
//! Data points must be sorted on their abscissa.
//!
//! **FR5** - If the supplied arguments have different length or have less than
//! three points, the algorithm returns and error.
//! If multiple points with the same abscissa are supplied, the result is undefined.
//!
//! # Test cases (TC)
//!
//! **TC1** - Test FR5, with zero points, result shall be an error
//!
//! **TC2** - Test FR5, with one point, result shall be an error
//!
//! **TC3** - Test FR5, with two points, result shall be an error
//!
//! **TC4** - Test the interpolation of the bisector of the 1st and 3rd quadrant,
//! interpolated function shall have the same abscissa and ordinate.
//!
//! **TC5** - Test a constant segment between two point with the same ordinate.
//!
//! # Minumum rust version
//!
//! The minimum rust compiler vesrsion is 1.44
//!
//! # References
//!
//! \[1\] M. Steffen, A Simple Method for Monotonic Interpolation in One Dimension, Astron. Astrophys. 239, 443-450 (1990)
//!
//! \[2\] L. L. Schumaker, On Shape Preserving Quadratic Spline Interpolation, SIAM J. Numer. Anal., Vol. 20, No. 4, August 1983
//!
//! \[3\] J. M. Hyman, Accurate Monotonicity Preserving Cubic Interpolation, SIAM J. Sci. Stat. Comput., Vol. 4, No. 4, December 1983
//!
//! \[4\] F. N. Fritsch and R. E. Carlson, Monotone Piecewise Cubic Interpolation, SIAM J. Numer. Anal., Vol. 17, No. 2, April 1980

#![warn(missing_docs)]

mod error;
mod interpolator;

pub use crate::{
    error::{Error, ErrorKind},
    interpolator::Mcspline,
};

/// Trait for numeric constant one
pub trait One {
    /// One
    fn one() -> Self;
}

/// Trait for numeric constant zero
pub trait Zero {
    /// Zero
    fn zero() -> Self;
}

/// Trait for absolute value
pub trait Abs {
    /// Absolute value
    fn abs(&self) -> Self;
}

/// Trait for the sign of the number
pub trait Signum {
    /// Returns a number that represents the sign of self.
    /// * 1.0 if the number is positive or +0
    /// * -1.0 if the number is negative or -0
    fn signum(&self) -> Self;
}

/// Trait for the minumum between two numbers
pub trait Min {
    /// Return the miminum between two numbers
    fn min(self, other: Self) -> Self;
}

/// Trait for special multiplication and addition
pub trait MulAdd {
    /// Return type
    type Output;
    /// Return self*a + b
    fn mul_add(&self, a: &Self, b: &Self) -> Self::Output;
}

use std::{
    cmp::PartialOrd,
    ops::{Add, Div, Mul, Sub},
};

/// Define all the required operation needed for the numerical type
pub trait Ops<T>:
    Abs
    + Add<Output = T>
    + for<'a> Add<&'a T, Output = T>
    + Clone
    + Div<Output = T>
    + for<'a> Div<&'a T, Output = T>
    + Min
    + Mul<Output = T>
    + for<'a> Mul<&'a T, Output = T>
    + MulAdd<Output = T>
    + One
    + PartialOrd
    + Signum
    + Sub<Output = T>
    + for<'a> Sub<&'a T, Output = T>
    + Zero
{
}

macro_rules! float_impl {
    ($($a:ident)*) => ($(
        impl One for $a {
            fn one() -> Self { 1. }
        }
        impl Zero for $a {
            fn zero() -> Self { 0. }
        }
        impl Abs for $a {
            fn abs(&self) -> Self { $a::abs(*self) }
        }
        impl Signum for $a {
            fn signum(&self) -> Self { $a::signum(*self) }
        }
        impl Min for $a {
            fn min(self, other:Self) -> Self { $a::min(self, other) }
        }
        impl MulAdd for $a {
            type Output = Self;
            fn mul_add(&self, a: &Self, b: &Self) -> Self::Output {
                $a::mul_add(*self, *a, *b)
            }
        }
        impl Ops<$a> for $a {}
    )*);
}

float_impl!(f32 f64);

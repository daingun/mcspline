use mcspline::{ErrorKind, Mcspline};

/// TC1
#[test]
fn empty_arguments() {
    let xs: Vec<f32> = vec![];
    let ys = vec![];
    let mcs = Mcspline::new(&xs, &ys);
    assert!(mcs.is_err());
    assert_eq!(ErrorKind::NotEnoughData, mcs.unwrap_err().kind());
}

/// TC2
#[test]
fn one_point() {
    let xs: Vec<f32> = vec![1.];
    let ys = vec![1.];
    let mcs = Mcspline::new(&xs, &ys);
    assert!(mcs.is_err());
    assert_eq!(ErrorKind::NotEnoughData, mcs.unwrap_err().kind());
}

/// TC3
#[test]
fn two_points() {
    let xs: Vec<f32> = vec![1., 2.];
    let ys = vec![1., 2.];
    let mcs = Mcspline::new(&xs, &ys);
    assert!(mcs.is_err());
    assert_eq!(ErrorKind::NotEnoughData, mcs.unwrap_err().kind());
}

/// TC4
#[allow(clippy::float_cmp)]
#[test]
fn three_points() {
    let xs: Vec<f32> = vec![1., 2., 3.];
    let ys = vec![1., 2., 3.];
    let mcs = Mcspline::new(&xs, &ys).unwrap();
    let mut x = 1.;
    while x <= 3. {
        assert_eq!(x, mcs.get_unbound(x));
        x += 0.01;
    }
}

/// TC5
#[allow(clippy::float_cmp)]
#[test]
fn constant_test() {
    let xs = vec![0.0, 1.0, 2.0, 3.0];
    let ys = vec![0.0, 2.0, 2.0, 0.0];
    let mcs = Mcspline::new(&xs, &ys).unwrap();
    let mut x = 1.;
    while x <= 2. {
        assert_eq!(2., mcs.get_unbound(x));
        x += 0.01;
    }
}
